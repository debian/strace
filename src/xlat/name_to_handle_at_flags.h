/* Generated by ./src/xlat/gen.sh from ./src/xlat/name_to_handle_at_flags.in; do not edit. */

#include "gcc_compat.h"
#include "static_assert.h"

#undef XLAT_PREV_VAL

#ifndef XLAT_MACROS_ONLY

# ifdef IN_MPERS

#  error static const struct xlat name_to_handle_at_flags in mpers mode

# else

DIAG_PUSH_IGNORE_TAUTOLOGICAL_CONSTANT_COMPARE
static const struct xlat_data name_to_handle_at_flags_xdata[] = {
 XLAT(AT_HANDLE_MNT_ID_UNIQUE),
 #define XLAT_VAL_0 ((unsigned) (AT_HANDLE_MNT_ID_UNIQUE))
 #define XLAT_STR_0 STRINGIFY(AT_HANDLE_MNT_ID_UNIQUE)
 XLAT(AT_HANDLE_CONNECTABLE),
 #define XLAT_VAL_1 ((unsigned) (AT_HANDLE_CONNECTABLE))
 #define XLAT_STR_1 STRINGIFY(AT_HANDLE_CONNECTABLE)
 XLAT(AT_HANDLE_FID),
 #define XLAT_VAL_2 ((unsigned) (AT_HANDLE_FID))
 #define XLAT_STR_2 STRINGIFY(AT_HANDLE_FID)
 XLAT(AT_SYMLINK_FOLLOW),
 #define XLAT_VAL_3 ((unsigned) (AT_SYMLINK_FOLLOW))
 #define XLAT_STR_3 STRINGIFY(AT_SYMLINK_FOLLOW)
 XLAT(AT_EMPTY_PATH),
 #define XLAT_VAL_4 ((unsigned) (AT_EMPTY_PATH))
 #define XLAT_STR_4 STRINGIFY(AT_EMPTY_PATH)
};
static
const struct xlat name_to_handle_at_flags[1] = { {
 .data = name_to_handle_at_flags_xdata,
 .size = ARRAY_SIZE(name_to_handle_at_flags_xdata),
 .type = XT_NORMAL,
 .flags_mask = 0
#  ifdef XLAT_VAL_0
  | XLAT_VAL_0
#  endif
#  ifdef XLAT_VAL_1
  | XLAT_VAL_1
#  endif
#  ifdef XLAT_VAL_2
  | XLAT_VAL_2
#  endif
#  ifdef XLAT_VAL_3
  | XLAT_VAL_3
#  endif
#  ifdef XLAT_VAL_4
  | XLAT_VAL_4
#  endif
  ,
 .flags_strsz = 0
#  ifdef XLAT_STR_0
  + sizeof(XLAT_STR_0)
#  endif
#  ifdef XLAT_STR_1
  + sizeof(XLAT_STR_1)
#  endif
#  ifdef XLAT_STR_2
  + sizeof(XLAT_STR_2)
#  endif
#  ifdef XLAT_STR_3
  + sizeof(XLAT_STR_3)
#  endif
#  ifdef XLAT_STR_4
  + sizeof(XLAT_STR_4)
#  endif
  ,
} };
DIAG_POP_IGNORE_TAUTOLOGICAL_CONSTANT_COMPARE

#  undef XLAT_STR_0
#  undef XLAT_VAL_0
#  undef XLAT_STR_1
#  undef XLAT_VAL_1
#  undef XLAT_STR_2
#  undef XLAT_VAL_2
#  undef XLAT_STR_3
#  undef XLAT_VAL_3
#  undef XLAT_STR_4
#  undef XLAT_VAL_4
# endif /* !IN_MPERS */

#endif /* !XLAT_MACROS_ONLY */
